#!/usr/bin/env bash

# ensure that tmp directory is owned by www-data
chown -R 33:33 ./apache/tmp

# ensure that the webroot is owned by www-data
chown -R 33:33 ./apache/html/

# ensure that the nginx access log is empty
echo "" > ./nginx/logs/access.log
